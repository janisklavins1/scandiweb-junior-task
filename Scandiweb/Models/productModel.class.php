<?php
require 'Db.class.php';
require 'Products/Dvd.php';
require_once 'product.abstract.php';
require_once 'Products/Dvd.php';
require_once 'Products/Furniture.php';
require_once 'Products/Book.php';

use Model\Database\Db;
use Model\Product\Product;

    class ProductModel extends Db{
           
        private $product = array();
        
        
        public function setProducts(){
  
            $getProductsSql = "SELECT * FROM product";

            $statement = $this->connectToDb()->prepare($getProductsSql);
            $statement->execute();
            
            $items = $statement->fetchAll();
            
            
            foreach ($items as $row) {
                
                if ($row['product_type_id'] == 1) {
                    $this->product[] = new Dvd($row['product_id'],$row['sku'], $row['name'], $row['price'], $row['product_type_id'], $row['size']);
                    
                }
                elseif ($row['product_type_id'] == 3) {
                    $this->product[] = new Furniture($row['product_id'], $row['sku'], $row['name'], $row['price'], $row['product_type_id'], $row['height'], $row['width'], $row['length']);
                }
                elseif ($row['product_type_id'] == 2) {
                    $this->product[] = new Book($row['product_id'], $row['sku'], $row['name'], $row['price'], $row['product_type_id'], $row['weight']);
                }
            }
            
            return $this->product;
        }

        public function prepare(Product $product,  $sku, $name, $price, $productTypeId, $size, $weight, $height, $width, $length){
 
            return $product->prepareProductForDb($sku, $name, $price, $productTypeId, $size, $weight, $height, $width, $length);
        
        }

        protected function setProduct($products){
           
            $insertProductSQL = "INSERT INTO product (sku, name, price, product_type_id, size, weight, height, width, length) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $statement = $this->connectToDb()->prepare($insertProductSQL);
            $statement->execute($products);

            
   
        }

        protected function deleteProduct($productId){

 
            $deleteProductSQL = "DELETE FROM product WHERE product_id = ? ";
            $statement = $this->connectToDb()->prepare($deleteProductSQL);
            $statement->execute([$productId]);

       
   
        }


    }
    
     


?>