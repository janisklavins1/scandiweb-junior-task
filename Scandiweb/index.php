<?php 
require_once ('Controllers/productsController.class.php');
require_once ('Views/productsView.class.php');

if (isset($_POST['btnDelete'])) {
           
    if(isset($_POST['delete'])){

        $pController = new ProductsController();        
        $pController->delProduct($_POST['delete']);

    }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Junior Test Jānis Kļaviņš</title>
    <link rel="stylesheet" href="style.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>

    <div class="container-xl mt-3">
    <form method="post">
        <div class="row justify-content-between">

            
            <div class="col-4">
                <h1>Product List</h1> 
            </div>
            <div class="col-4 ">

                <!-- <a href="add-product.php" class="btn btn-success btn active" role="button" aria-pressed="true">ADD</a> -->
                <!-- <input class="btn btn-success" type="button" onclick="window.location.href='./add-product.php'" value="ADD"> -->
                <a href="add-product.php">
                    <button class="btn btn-success" type="button" >ADD</button>
                </a>
                <!-- <button class="btn btn-success" onclick="window.location.href='./add-product.php'" >ADD</button>  -->
                <button type="submit" class="btn btn-danger" id="delete-product-btn" name="btnDelete" >MASS DELETE</button> 
            </div>
            
        </div>

        <hr>

        <div class="row mt-4"><hr>
            <?php
                
                $products = new ProductsView();

               
                $allProducts = $products->showProducts();
                // echo "<h3>".$allProducts[0]['name']."</h3>";
                
                if (count($allProducts) > 0) {
                    
                    foreach ($allProducts as $product) {
                        
                        ?>
							<div class="col-3 mb-3"> 
								<div class=" p-4 border-card text">
                                <input type="checkbox" class="delete-checkbox" name="delete[]" value='<?= $product->getId()?>' >
                                    <?php                                   
                                        echo "<p>".$product->getSku()."</p>";	
										echo "<p>".$product->getName()."</p>";										
										echo "<p>".$product->getPrice()." $</p>";                                       									
										echo "<p>".$product->getAdditionalInfo()."</p>";
									?>

								</div>
							</div>
							<?php
                    }

                }
            ?>
        </div>
                <hr>
        <div class="row text">
            <div class="col text-center ">
                <p class="">Scandiweb Test assignment</p>
            </div>
                
        </div>
        
        </form>
            	
	</div>
</body>
</html>