<?php 
require_once ('Controllers/productsController.class.php');


if (isset($_POST['btnSave'])) { 
    $pController = new ProductsController();  
    $pController->createProduct($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['select'], $_POST['size'], $_POST['weight'], $_POST['height'], $_POST['width'], $_POST['length']);
    
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Junior Test Jānis Kļaviņš</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" crossorigin="anonymous">
    <script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
 
    
</head>
<body>

    <div class="container-xl mt-3">
    <form method="post" id="product_form">
        <div class="row justify-content-between">

            
            <div class="col-4">
                <h1>Product Add</h1> 
            </div>
                <div class="alert alert-danger" role="alert" style="visibility: hidden;" id="errorMessage">
                    
                </div>               
            <div class="col-4 ">
               
                <button type="submit" method="post" class="btn btn-success btn active" name="btnSave" id="btnSave">Save</button> 
                
                <a href="index.php" class="btn btn-danger btn active" role="button" aria-pressed="true">Cancel</a>                             
            </div>
            
        </div>

        <hr>

        <div class="row mt-4 align-items-start"><hr>

            <div class="col ">

                <div class="form-group row">
                    <label  class="col-sm-1 col-form-label">SKU</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="sku" name="sku" >
                    </div>
                </div>

                <div class="form-group row">
                    <label  class="col-sm-1 col-form-label">Name</label>
                    <div class=" col-sm-4">
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="form-group row">
                    <label  class="col-sm-1 col-form-label">Price</label>
                    <div class=" col-sm-4">
                        <input type="number" min="0" step="0.01" class="form-control" id="price" name="price">
                    </div>
                </div>

                <div class="form-group row">
                    <label  class="col-sm-2 col-form-label">Type Switcher</label>
                    <div class=" col-sm-2">

                    <select class="form-control" name="select" id="productType">
                        <option disabled selected >Type Switcher</option>
                        <option value="Dvd">DVD</option>
                        <option value="Furniture" >Furniture</option>
                        <option value="Book" >Book</option>
                    </select>

                    </div>
                </div>

                <div class="option-grouping" id="Dvd" style="display:none;">
                    <div class="form-group row">
                        <label  class="col-sm-1 col-form-label">Size (MB)</label>
                        <div class=" col-sm-4">
                            <input type="number" min="0" step="1" class="form-control" id="size" name="size">
                        </div>
                    </div>
                    <p class="font-weight-bold">Please, provide size</p>
                </div>

                <div class="option-grouping" id="Furniture" style="display:none;">
                    <div class="form-group row">
                        <label  class="col-sm-1 col-form-label">Height (CM)</label>
                        <div class=" col-sm-4">
                            <input type="number" min="0" step="0.01" class="form-control" id="height" name="height"> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-sm-1 col-form-label">Width (CM)</label>
                        <div class=" col-sm-4">
                            <input type="number" min="0" step="0.01" class="form-control" id="width" name="width">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-sm-1 col-form-label">Length (CM)</label>
                        <div class=" col-sm-4">
                            <input type="number" min="0" step="0.01" class="form-control" id="length" name="length">
                        </div>
                    </div>
                    <p class="font-weight-bold">Please, provide dimensions</p>
                </div>
                
                <div class="option-grouping " id="Book" style="display:none;">
                    <div class="form-group row">
                        <label  class="col-sm-1 col-form-label">Weight (KG)</label>
                        <div class=" col-sm-4">
                            <input type="number" min="0" step="0.01" class="form-control" id="weight" name="weight">
                        </div>
                    </div>
                    <p class="font-weight-bold">Please, provide weight</p>
                </div>
                

                <script>
                    $('#productType').on('change', function() {
                        var category = $(this).val();
                        //Hides all the divs
                        $('.option-grouping').hide();
                        //Unhides the selected options
                        $('#' + category).show();
                    });
                </script>
                
            </div>

        </div>
                <hr>
        <div class="row text">
            <div class="col text-center ">
                <p class="">Scandiweb Test assignment</p>
            </div>
                
        </div>
        
        </form>
            	
	</div>
</body>
<script src="validation.js"></script>
</html>